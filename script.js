document.ondragstart = function() {return false}

document.onselectstart = function() {return false}

$(document).ready(function(){
    var headerSlider = true;
    var interval = true;
    var number = 1;
    var appSlide = true;
    var mouse = {x:-1,X:-1};
    var slideActive = [-1,''];
    $(".header__menu").css({top:-$(".header").height(),bottom:$(".header").height()});

    $("button[data-scroll]").click(function(e){
        e.preventDefault();
        var block = $(this).attr("data-scroll");
        var value = $(".container__app."+block).offset().top;
        if($(this).attr("data-version")) $(".menu-btn").click();
        $("html,body").animate({scrollTop:value+"px"},700,'swing');
    });


    $("div[data-marker]").click(function(){
        if(headerSlider) {
            headerSlider = false;
            interval = false;
            var elem = $(this);
            var number = parseInt($(this).attr("data-marker"));
            $("div.show[data-header-slide]").animate({opacity: 0}, 500, 'swing', function () {
                $("div[data-marker]").removeClass("active");
                elem.addClass("active");
                $(this).removeClass("show").addClass("hide");
                $("div[data-header-slide='" + number + "']").removeClass("hide").addClass("show").animate({opacity: 1}, 500, 'swing',function(){
                    setTimeout(function(){
                        headerSlider = true;
                    },250);
                });
            });
        }
    });

    $(".phone-slider[data-app='photoreport']").bxSlider({
        mode: 'fade',
        pagerType:'full',
        nextSelector:".btn-arrow.right.photoreport",
        nextText:'',
        prevSelector:".btn-arrow.left.photoreport",
        prevText:'',
        preventDefaultSwipeY:false
    });

    $(".phone-slider[data-app='voting-day']").bxSlider({
        mode: 'fade',
        pagerType:'full',
        nextSelector:".btn-arrow.right.voting-day",
        nextText:'',
        prevSelector:".btn-arrow.left.voting-day",
        prevText:'',
        preventDefaultSwipeY:false
    });

    $(".phone-slider[data-app='gps-tracker']").bxSlider({
        mode: 'fade',
        pagerType:'full',
        nextSelector:".btn-arrow.right.gps-tracker",
        nextText:'',
        prevSelector:".btn-arrow.left.gps-tracker",
        prevText:'',
        preventDefaultSwipeY:false
    });

    $(".phone-slider[data-app='agitator']").bxSlider({
        mode: 'fade',
        pagerType:'full',
        nextSelector:".btn-arrow.right.agitator",
        nextText:'',
        prevSelector:".btn-arrow.left.agitator",
        prevText:'',
        preventDefaultSwipeY:false
    });

    $(".menu-btn").click(function(){
        if($(this).attr("class").indexOf("active") == -1){
            $(this).addClass("active");
            $(".header__menu").removeClass("hide").addClass("show");
            $(".header__menu").animate({bottom:0,top:$("._title").height()+3},1000,'swing');
            setTimeout(function(){
                $(".header__color-block").addClass("active");
            },400);
        }else{
            $(".header__menu").animate({bottom:$(".header").height(),top:-$(".header").height()},1000,'swing',function(){
                $(".header__menu").removeClass("show").addClass("hide");
            });
            setTimeout(function(){
                $(".header__color-block").removeClass("active");
            },400);
            $(this).removeClass("active");
        }
    });

    $(".open-text").click(function(){
        var value = $(this).attr("data-block");
        $(this).animate({opacity:0},300);
        $(".container__app."+value).animate({height:1295},500,'swing');
        $("div[data-open='"+value+"']").animate({height:500},500,'swing');
    });

    setInterval(function(){
        if(interval){
            number += 1;
            if(number > $("div[data-marker]").length){
                number = 1;
            }
            var nm = $("div.active[data-marker]").attr("data-marker");
            $("div.active[data-marker]").removeClass("active");
            $("div[data-marker='"+number+"']").addClass("active");
            headerSlider = false;
            $("div.show[data-header-slide='"+nm+"']").animate({opacity: 0}, 500, 'swing', function () {
                $(this).removeClass("show").addClass("hide");
                $("div[data-header-slide='" + number + "']").removeClass("hide").addClass("show").animate({opacity: 1}, 500, 'swing',function(){
                    headerSlider = true;
                });
            });
        }
    },10000);
});